const altProns = [
    "zju:mə",
    "s(j)u:mɛk"
]

function main() {

    randPron()

    for ( var page of document.querySelectorAll('container > div') ) {
        observer.observe(page)
    }

    navbarSelector()

}

function randPron() {

    if (random(100) != 0) return

    var pron = document.querrySelector("#pronunciation")
    pron.innerText = altProns[random(altProns.length)]

}

function random(max) {
    return Math.floor(Math.random() * max);
}


const options = {
    rootMargin: '100px',
    threshold: 0.5
}

function callback(entries) {

    for (let entry of entries) {

        if (!entry.isIntersecting) return

        const id = entry.target.id
        history.pushState(null,null,'#'+id)
        navbarSelector()
        // the top 2 lines can be replaced with:
        // window.location.hash = entry.target.id

    }

}

let observer = new IntersectionObserver(callback, options)

function navbarSelector() {
    const id = window.location.hash.split('#')[1]
    const navs = document.querySelectorAll('navbar > a')
    for (let nav of navs) {
        console.log(nav.id === id+'Nav')
        if ( nav.id === id+'Nav' ) nav.classList.add('selected')
        else nav.classList.remove('selected')
    }
}

window.onhashchange = () => navbarSelector()